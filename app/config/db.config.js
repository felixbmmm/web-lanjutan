module.exports = {
    HOST: "localhost",
    USER: "nodejsapp",
    PASSWORD: "nodejsapp123",
    DB: "nodejs_auth_testing",
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};