module.exports = (sequelize, Sequelize) => {
    const Item = sequelize.define("items", {
        name: {
            type: Sequelize.STRING
        },
        stock: {
            type: Sequelize.INTEGER
        }
    });

    return Item;
};