const { authJwt } = require("../middleware");
const itemController = require("../controllers/item.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/items",
        [authJwt.verifyToken],
        itemController.getAll
    );

    app.get(
        "/api/items/:itemId",
        [authJwt.verifyToken],
        itemController.getById
    );

    app.post(
        "/api/items",
        [authJwt.verifyToken],
        itemController.createItem
    );

    app.put(
        "/api/items/:itemId",
        [authJwt.verifyToken],
        itemController.updateItem
    );

    app.delete(
        "/api/items/:itemId",
        [authJwt.verifyToken, authJwt.isAdmin],
        itemController.deleteItem
    );
};