const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;

exports.allAccess = (req, res) => {
    res.status(200).send({
        message: "Public Content."
    });
};

exports.userBoard = (req, res) => {
    res.status(200).send({
        message: "User Content."
    });
};

exports.adminBoard = (req, res) => {
    res.status(200).send({
        message: "Admin Content."
    });
};

exports.moderatorBoard = (req, res) => {
    res.status(200).send({
        message: "Moderator Content."
    });
};

exports.getById = (req, res) => {
    const id = req.params.userId;

    User.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Internal Server Error, " + err
            });
        });
};