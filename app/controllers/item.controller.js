const db = require("../models");
const Item = db.item;
const Op = db.Sequelize.Op;

exports.getAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

    const items = Item.findAll({ where: condition });
    res.status(200).send({
        data: items
    });
};

exports.getById = (req, res) => {
    const id = req.params.itemId;

    Item.findByPk(id)
        .then(data => {
            res.status(200).send({
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Internal Server Error, " + err
            });
        });
};

exports.createItem = (req, res) => {
    Item.create({
        name: req.body.name,
        stock: req.body.stock
    })
    .then(item => {
        res.status(200).send({
            data: item
        });
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    })
    
};

exports.updateItem = (req, res) => {
    Item.update({
        name: req.body.name,
        stock: req.body.stock
    }, {
        where: {
            id: req.params.itemId
        }
    })

    res.status(200).send({
        message: "Item successfully updated"
    });
};

exports.deleteItem = (req, res) => {
    Item.destroy({
        where: {
            id: req.params.itemId
        }
    });

    res.status(204).send();
};