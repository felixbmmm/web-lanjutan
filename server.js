const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http")
const fs = require("fs")

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
const Role = db.role;
const User = db.user;

db.sequelize.sync({force: true}).then(() => {
    console.log('Drop and Resync Db');
    initial();
});

function initial() {
    Role.create({
        id: 1,
        name: "user"
    });

    Role.create({
        id: 2,
        name: "moderator"
    });

    Role.create({
        id: 3,
        name: "admin"
    });
}

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Hi." });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/item.routes')(app);

// set port, listen for requests
app.listen(8000, () => {
    console.log("Server is running on port 8000.");
});


// http.createServer(function (req, res) {
//     fs.readFile("index.html", (err, data) => {
//         if (err) throw err;

//         res.writeHead(200, { "Content-Type" : "text/html" });

//         res.write(data)

//         res.end();
//     })
// })
// .listen(8100)